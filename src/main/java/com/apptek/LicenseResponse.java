package com.apptek;

import lombok.Data;

@Data
public class LicenseResponse {
    final private String token;
    final private Payload payload;

    @Data
    public static class Payload {
        // Undocumented
        private String accountEmail;
        private String accountNumber;
        private String apiKey;
        private String logText;
        private String saveData;
        // Documented
        private long exp;
        private String host;
        private long iat;
        private String iss;
        private String jti;
        private long maxChannels;
        private long maxMinutes;
        private int port;
        private String task;
    }
}


