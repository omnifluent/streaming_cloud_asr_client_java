package com.apptek;

import com.google.protobuf.AbstractMessage;
import com.google.protobuf.ByteString;
import gateway.Apptek;
import gateway.GatewayGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class Application {

    private static final String LICENSE_SERVER_URL = "https://license.apptek.com";
    private static final String TEST_FILE = "sample1.wav";

    private LicenseResponse licenseResponse;
    private ManagedChannel channel;

    public static void main(String[] args) throws InterruptedException, IOException, URISyntaxException {

        if (args.length < 1) {
            System.err.println("Usage:");
            System.err.println("mvn exec:java -Dexec.args=\"<license-key>\"");
            System.exit(1);
        }
        Application app = new Application();

        app.init(args[0]);
        app.checkAvailability();

        StreamRecognizeResponseStreamObserver observer = new StreamRecognizeResponseStreamObserver();

        app.processFile(TEST_FILE, observer);

        try {
            CompletableFuture<String> feature = observer.await();
            feature.get();
            System.out.println("We're done");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        app.shutdown();
    }

    private void shutdown() {
        channel.shutdown();
    }

    private void processFile(String filename, StreamObserver<Apptek.RecognizeStreamResponse> responseStreamObserver) {
        GatewayGrpc.GatewayStub stub = GatewayGrpc.newStub(channel);

        StreamObserver<Apptek.RecognizeStreamRequest> input = stub.recognizeStream(responseStreamObserver);

        Apptek.RecognizeStreamRequest req = Apptek.RecognizeStreamRequest.newBuilder()
                .setConfiguration(Apptek.RecognizeStreamConfig.newBuilder()
                        .setAudioConfiguration(Apptek.RecognizeAudioConfig.newBuilder()
                                .setEncoding(Apptek.RecognizeAudioEncoding.PCM_16bit_SI_MONO)
                                .setSampleRateHz(8000)
                                .setLangCode("en-US")
                                .build())
                        .setLicenseToken(licenseResponse.getToken())
                        .build()).build();
        input.onNext(req);


        try (FileInputStream audio = new FileInputStream(filename)) {
            byte[] buffer = new byte[1024];
            int size;

            while ((size = audio.read(buffer)) > 0) {
                req = Apptek.RecognizeStreamRequest.newBuilder().setAudio(ByteString.copyFrom(buffer, 0, size)).build();
                input.onNext(req);
            }

            // Important to signal end of input
            input.onCompleted();
        } catch (Exception e) {
            // Important to report an error
            input.onError(e);
            e.printStackTrace();
        }
    }

    private void checkAvailability() {
        GatewayGrpc.GatewayBlockingStub blockingStub = GatewayGrpc.newBlockingStub(channel);

        Apptek.RecognizeAvailableResponse availableResponse = blockingStub.recognizeGetAvailable(Apptek.AvailableRequest.newBuilder().setLicenseToken(licenseResponse.getToken()).build());
        String list = availableResponse.getListList().stream().map(AbstractMessage::toString).collect(Collectors.joining(","));
        System.out.println(list);
    }

    private void init(String license) throws IOException, URISyntaxException {

        String jwt = getToken(license);

        licenseResponse = tokenToResponse(jwt);
        channel = ManagedChannelBuilder
                .forTarget(licenseResponse.getPayload().getHost() + ":" + licenseResponse.getPayload().getPort())
                .build();
    }

    private String getToken(String apiKey) throws IOException, URISyntaxException {
        URIBuilder builder = new URIBuilder(LICENSE_SERVER_URL);
        builder.setPathSegments("license", "v2", "token", apiKey);
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(builder.build());
            try (CloseableHttpResponse licenseResponse = httpclient.execute(httpGet)) {
                HttpEntity entity1 = licenseResponse.getEntity();
                return EntityUtils.toString(entity1, "UTF-8");
            }
        }
    }

    protected LicenseResponse tokenToResponse(String token) {
        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getDecoder();
        String payloadString = new String(decoder.decode(chunks[1]));

        LicenseResponse.Payload payload = CustomJsonMapper.readUglyValue(payloadString, LicenseResponse.Payload.class);
        return new LicenseResponse(token, payload);

    }


    private static class StreamRecognizeResponseStreamObserver implements StreamObserver<Apptek.RecognizeStreamResponse> {

        private Apptek.RecognizeTranscription lastSegment;

        private final CompletableFuture<String> asrFeature;

        public StreamRecognizeResponseStreamObserver() {
            asrFeature = new CompletableFuture<>();
        }

        public CompletableFuture<String> await() {
            return asrFeature;
        }

        @Override
        public void onNext(Apptek.RecognizeStreamResponse streamRecognizeResponse) {
            if (streamRecognizeResponse.hasTranscription()) {

                Apptek.RecognizeTranscription transcription = streamRecognizeResponse.getTranscription();
                if (lastSegment == null)
                    lastSegment = transcription;

                if (lastSegment.getSegment().getId() != transcription.getSegment().getId()) {
                    System.out.println();
                }

                System.out.print("\r");
                System.out.print(transcription.getSegment().getId() + ":" + transcription.getOrth());

                lastSegment = transcription;
            }
        }

        @Override
        public void onError(Throwable throwable) {
            System.out.println();
            System.out.println("error " + throwable.getMessage());
            throwable.printStackTrace();
            asrFeature.completeExceptionally(throwable);
        }

        @Override
        public void onCompleted() {
            System.out.println();
            System.out.println("completed");
            asrFeature.complete(null);
        }
    }
}
