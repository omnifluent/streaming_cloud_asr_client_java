package com.apptek;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;

public class CustomJsonMapper {

    static final public ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    public static <T> T readUglyValue(String data, Class<T> cls) {
        try {
            return mapper.readValue(data, cls);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("error parsing", e);
        }
    }
}
